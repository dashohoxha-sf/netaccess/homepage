#!/bin/bash
### create a demo of netaccess by applying the patch

### go to this directory
cd $(dirname $0)

### get a copy of netaccess
svn export https://svn.sourceforge.net/svnroot/netaccess/trunk .. --force

### apply the patch to it
cd ..
patch -p1 -ulE -i diff/demo.diff

### add a symlink to web_app
ln -s ../web_app/ .

### make executable all the script files
find . -name '*.sh' | xargs chmod +x

### install and init the database
db/init.sh

### compile translation files to binary format
langs="en sq_AL nl it de"
for lng in $langs
do
  l10n/msgfmt.sh $lng
done

