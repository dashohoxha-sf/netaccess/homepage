#!/bin/bash
### make a patch with the modifications of the demo wrt the trunk

cd $(dirname $0)

rm -rf netaccess/
svn export https://svn.sourceforge.net/svnroot/netaccess/trunk netaccess

find .. -name '*~' | xargs rm

diff -rubB netaccess .. > demo.diff

rm -rf netaccess/

