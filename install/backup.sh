#!/bin/bash

cd $(dirname $0)
cd ../..

tar --create --preserve --absolute-names --gzip \
    --file=netaccess-htdocs.tar.gz \
    htdocs/ /tmp/persistent/netaccess/

