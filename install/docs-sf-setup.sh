#!/bin/bash
### this script is used to setup the application at SF
### so that some files and directories can be written by apache

tmp_path=/tmp/persistent/smewebapp/docs

### the list of files
app_files="templates/menu/menu.xml
           templates/menu/menu_items.js
           templates/menu/book_list.php
           templates/admin/edit_menu/menu/menu.xml"

app_dirs="templates/admin/access_rights
          content/books
          content/workspace
          content/initial_xml/uploaded"

### update
if [ "$1" = "update" ]
then
  for fname in $app_files $app_dirs ; do rm -f $fname ; done
  svn update
  exit 0
fi

### move $app_files and $app_dirs in $tmp_path and link here
for fname in $app_files $app_dirs
do
  dir=$(dirname $fname)
  mkdir -p $tmp_path/$dir
  mv -f $fname $tmp_path/$dir/
  ln -sf $tmp_path/$fname $fname
done

### make them writable by anybody (including nobody/apache)
find $tmp_path -type d -exec chmod go+ws {} \;
find $tmp_path -type f -exec chmod go+w {} \;

### remove the .svn directories
find $tmp_path -name '\.svn' -exec rm -rf {} \;

