#!/bin/bash
### install the homepage, the demo app, docs, etc. for NetAccess

### go to htdocs/
cd $(dirname $0)
cd ..

### get the variable $svnroot
. ~dashohoxha/svnroot.sh

### checkout projects
svn checkout $svnroot/netaccess/homepage .
svn checkout $svnroot/phpwebapp/web_app/trunk web_app

### install demo
demo/diff/mk-demo.sh

### install docs/
#install/docs.sh

