<?php
/*
Copyright 2001,2002,2003 Dashamir Hoxha, dashohoxha@users.sourceforge.net

This file is part of phpWebApp.

phpWebApp is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

phpWebApp is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with phpWebApp; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA  
*/


class content extends WebObject
{
  function onParse()
    {
      //set the {{content_file}} according to the selected tab
      $tab = WebApp::getSVar("tabs1::mainMenu->selected_item");
      switch ($tab)
        {
        default:
        case "scarlet":
          $content_file = "scarlet.html";
          break;
        case "install":
          $content_file = "install.html";
          break;
        case "testing":
          $content_file = "testing.html";
          break;
        case "manual":
          $content_file = "manual.html";
          WebApp::setSVar('docbook->cache_path', 'pages/manual_pages/');
          WebApp::setSVar('docbook->book_id', 'slackrouter');
          WebApp::setSVar('docbook->node_path', './');
          break;
        case "docs":
          $content_file = "docs.html";
          break;
        case "hsmng":
          $content_file = "hotspot-manager.html";
          break;
        case "hsmng-docs":
          $content_file = "hsmng-docs.html";
          WebApp::setSVar('docbook->cache_path', 'pages/hsmanager_docs/');
          WebApp::setSVar('docbook->book_id', 'hotspot_manager');
          WebApp::setSVar('docbook->node_path', './');
          break;
        case "hsmng-codedocs":
          $content_file = "hsmanager_codedocs/index.html";
          break;
        }
      WebApp::addVar("content_file", $content_file);
    }
}
?>
