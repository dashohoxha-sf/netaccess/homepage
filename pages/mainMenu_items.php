<?php
/** 
 * The $menu_items array contains the items of the mainMenu. 
 */
$menu_items = array(
                    "scarlet"  => "Scarlet",
                    "install"  => "Installation",
                    "testing"  => "Testing",
                    "manual"   => "Manual",
                    "docs"     => "Other Docs",
                    "hsmng"    => "HotSpot Manager",
                    "hsmng-docs" => "HSManager Docs",
                    "hsmng-codedocs" => "HSManager Code Docs"
                    );
?>
